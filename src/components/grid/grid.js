import './grid.css';
import React from 'react';
import axios from 'axios'

class DataTableHeader extends React.Component{
    render(){
      return(
        <th className="column">
          {this.props.columnNames}
        </th>
      )
    }
}

class DataTableRow extends React.Component{
    render(){
      return(
        <td>
          {this.props.cell}
        </td>
      )
    }
}

export default class DataTable extends React.Component{
  constructor(props){
    super(props);
    this.state = {products: [], columnNames: [], values: [] };

  }

  componentDidMount(){
    axios.get('/products.js').then(res=>{
      const products = res.data;
      this.setState( {products});

      if(products.length !== 0){
        var columnNames = Object.keys(products[0]);
        this.setState({columnNames})
      }
    })
  }

   createCells(fields, cssClass){

      return(
        <tr className={cssClass}>
          {
            fields.map(field=>{
              return (<DataTableRow cell={field} />);
            })
          }
        </tr>
        );
  }

  createRows(){
    let rows =[];
    for (var i = 0; i < this.state.products.length; i++)
    {
      let cssClass = "";

      if(i % 2 !== 0){
        cssClass = "altRow";
      }

      rows.push(this.createCells(Object.values(this.state.products[i]), cssClass));
    }

    return rows;
  }


        render(){
          return(
            <div >
              <table className="dataGrid">
                <thead>
                  <tr>
                    { this.state.columnNames.map(name=> <DataTableHeader columnNames={name} />) }
                  </tr>
                </thead>
                <tbody>
                  { this.createRows() }

                </tbody>
              </table>
              <div className="floatLeft"><span>Total Number of Records:</span>{this.state.products.length}</div>
              <div className="floatLeft">Page: </div>
            </div>
      )
    }
}
