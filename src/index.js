import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, Link } from "react-router-dom";
//import DataTable from './components/grid/grid.js';
import soldGrid from './screens/soldGrid.js';
import './index.css';

import registerServiceWorker from './registerServiceWorker';


const AppRouter = () => (
  <Router>
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/sold/">Sold Grid</Link>
          </li>
          <li>
            <Link to="/users/">Users</Link>
          </li>
        </ul>
      </nav>

      <Route path="/sold/" exact component={soldGrid} />
    </div>
  </Router>
);



ReactDOM.render(<AppRouter />, document.getElementById('root'));
registerServiceWorker();
